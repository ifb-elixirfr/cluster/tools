#! /usr/bin/env python

import glob
import json
import requests


package_conda_paths = glob.glob('../tools/*/*/env.yml')

packages = []
for package_conda_path in package_conda_paths:
    paths = package_conda_path.split("/")
    packages.append(paths[2])

packages = list((dict.fromkeys(packages)))

for package in packages:
    no_package_found = True
    for channel in ["bioconda","conda-forge"]:

        response = requests.get("https://api.anaconda.org/package/%s/%s" % (channel, package))

        if (response.status_code != 200):
            continue
        
        no_package_found = False
        package_dict = json.loads(json.dumps(response.json()))
        if ("linux-aarch64" in package_dict["platforms"]):
            print("%s\t%s" % (package, "linux-aarch64"))
            break
        else:
            print("%s\t%s" % (package, "no_supported"))
            break
        
    if (no_package_found):
        print("%s\t%s" % (package, "no_package_found"))
    