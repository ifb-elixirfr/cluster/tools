stages:
  - Test
  - Production

####### COMMON (.pre) #######

# Changes detection and lint
#
# parses modified files and determines tools to build or deploy
# check missing meta data or env/definition file
# lint modified meta data file
Changes detection:
  stage: .pre
  only:
    - merge_requests
    - master@ifb-elixirfr/cluster/tools
    - web
  tags:
    - docker
    - nncr
    - M
  image: python:3.6
  script:
    - bash ci/check_changes.sh
  artifacts:
    paths:
      - ci/*.list
  needs: []

####### MISC extensions #######

.deploy_conda_tools:
  only:
    - master@ifb-elixirfr/cluster/tools
    - web
  tags:
    - docker
    - nncr
    - M
  image: python:3.11-alpine
  before_script:
    - apk add bash git jq openssh
    - eval $(ssh-agent -s)
    - echo "$CLUSTER_SSH_KEY" | tr -d '\r' | ssh-add -
    - mkdir ~/.ssh
    - ssh-keyscan $CLUSTER_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo -e "Host *\n    ServerAliveInterval 30\n    ServerAliveCountMax 5" > ~/.ssh/config
    - pip3 install j2cli[yaml] yq
  script:
    - bash -c 'if [ ! -f ci/conda-tools.list ]; then echo "Error Artifact ci/conda-tools.list is missing"; exit 1; fi'
    - bash ci/conda/create_env.sh ci/conda-tools.list

.deploy_conda_tools_preproduction:
  extends: .deploy_conda_tools
  stage: Test
  tags:
    - docker
    - nncr
    - M
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: "/shared/software/miniconda"
    MODULEFILES_PATH: "/shared/software/modulefiles"
    CLUSTER_HOST: hpc.poc-nncr-ifb.fr
    #CLUSTER_HOST: slurm-client.ifb.local
    CLUSTER_SSH_KEY: $IFB_PREPROD_SSH_KEY
    CLUSTER_USER: root
  needs:
    - job: "Changes detection"
      artifacts: true

.deploy_conda_tools_production:
  stage: Production
  needs:
    - job: "Changes detection"
      artifacts: true
    - job: "IFB preprod Conda"

.deploy_singularity_tools:
  only:
    - master@ifb-elixirfr/cluster/tools
    - web
  tags:
    - docker
    - nncr
    - XL
  image:
    name: registry.gitlab.com/ifb-elixirfr/docker-images/apptainer:1.1.6
    entrypoint: [""]
  before_script:
    - eval $(ssh-agent -s)
    - echo "$CLUSTER_SSH_KEY" | tr -d '\r' | ssh-add -
    - mkdir ~/.ssh
    - ssh-keyscan $CLUSTER_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo -e "Host *\n    ServerAliveInterval 30\n    ServerAliveCountMax 5" > ~/.ssh/config
    - mkdir -p $SINGULARITY_REPOSITORY_PATH
    - sshfs $CLUSTER_USER@$CLUSTER_HOST:$SINGULARITY_REPOSITORY_PATH $SINGULARITY_REPOSITORY_PATH
    - mkdir -p $SINGULARITY_REPOSITORY_PATH/images
    - mkdir -p $SINGULARITY_REPOSITORY_PATH/wrappers
    - mkdir -p $MODULEFILES_PATH
    - sshfs $CLUSTER_USER@$CLUSTER_HOST:$MODULEFILES_PATH $MODULEFILES_PATH
  script:
    - bash ci/singularity/build_images.sh ci/singularity-tools.list

.deploy_singularity_tools_preproduction:
  extends: .deploy_singularity_tools
  stage: Test
  tags:
    - docker
    - nncr
    - XL
  variables:
    SINGULARITY_REPOSITORY_PATH: /shared/software/singularity
    SOFTWARE_SRC_PATH: /shared/software/singularity
    MODULEFILES_PATH: "/shared/software/modulefiles"
    CLUSTER_HOST: hpc.poc-nncr-ifb.fr
    #CLUSTER_HOST: slurm-client.ifb.local
    CLUSTER_SSH_KEY: $IFB_PREPROD_SSH_KEY
    CLUSTER_USER: root
  needs:
    - job: "Changes detection"
      artifacts: true

.deploy_singularity_tools_production:
  stage: Production
  needs:
    - job: "Changes detection"
      artifacts: true
    - job: "IFB preprod Singularity"


####### TEST MERGE RESQUEST #######

Conda test MR:
  stage: Test
  only:
    - merge_requests
  tags:
    - docker
    - nncr
    - M
  image: registry.gitlab.com/ifb-elixirfr/docker-images/mamba:0-27-0
  variables:
    REMOTE_MODE: "false"
    CONDA_HOME: /opt/conda
    MODULEFILES_PATH: /tmp
  before_script:
    - apt update -y
    - apt install -y jq git
    - pip install j2cli[yaml] yq
  script:
    - bash ci/conda/create_env.sh ci/conda-tools.list
  needs:
    - job: "Changes detection"
      artifacts: true

Singularity test MR:
  stage: Test
  only:
    - merge_requests
  tags:
    - docker
    - nncr
    - XL
  image:
    name: registry.gitlab.com/ifb-elixirfr/docker-images/apptainer:1.1.6
    entrypoint: [""]
  variables:
    SINGULARITY_REPOSITORY_PATH: /tmp/singularity
    MODULEFILES_PATH: /tmp/modulefiles
  before_script:
    - mkdir -p $SINGULARITY_REPOSITORY_PATH/images
    - mkdir -p $SINGULARITY_REPOSITORY_PATH/wrappers
  script:
    - bash ci/singularity/build_images.sh ci/singularity-tools.list
  needs:
    - job: "Changes detection"
      artifacts: true

IFB preprod Conda  test MR:
  extends: .deploy_conda_tools_preproduction
  only:
    - merge_requests
    - web
  when: manual

IFB dev Singularity test MR:
  extends: .deploy_singularity_tools_preproduction
  only:
    - merge_requests
    - web
  when: manual

####### CONDA Production ########

IFB preprod Conda:
  extends: .deploy_conda_tools_preproduction

IFB Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: "/shared/ifbstor1/software/miniconda"
    MODULEFILES_PATH: "/shared/ifbstor1/software/modulefiles"
    #CLUSTER_HOST: core.cluster.france-bioinformatique.fr
    CLUSTER_HOST: core-login2.cluster.france-bioinformatique.fr
    CLUSTER_SSH_KEY: $IFB_CLUSTER_SSH_KEY
    CLUSTER_USER: root

ABiMS Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /shared/software/miniconda
    MODULEFILES_PATH: /shared/software/modulefiles
    CLUSTER_HOST: slurm0.sb-roscoff.fr
    CLUSTER_SSH_KEY: $ABIMS_CLUSTER_SSH_KEY
    CLUSTER_USER: gitlab-runner

IGBMC Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /shared/software/miniconda3-admin
    MODULEFILES_PATH: /shared/software/modulefiles
    CLUSTER_HOST: hpc.igbmc.fr
    CLUSTER_SSH_KEY: $IGBMC_CLUSTER_SSH_KEY
    CLUSTER_USER: cs-hpc

CCUS Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  tags:
    - docker
    - igbmc # needed as it does only allow connection from osiris network
    - M
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /usr/local/ifb/miniconda3
    MODULEFILES_PATH: /usr/local/configfiles/ifb_modules
    CLUSTER_HOST: hpc-login.u-strasbg.fr
    CLUSTER_SSH_KEY: $CCUS_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb

BiRD Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /CONDAS/users/ifb-tools/miniconda3
    MODULEFILES_PATH: /CONDAS/shares/ifb-nncr/modulesfiles
    CLUSTER_HOST: bird2login.univ-nantes.fr
    CLUSTER_SSH_KEY: $BiRD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools

MCIA Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /gpfs/softs/contrib/ifb/miniconda3
    MODULEFILES_PATH: /gpfs/softs/contrib/ifb/modulefiles
    CLUSTER_HOST: curta.mcia.fr
    CLUSTER_SSH_KEY: $MCIA_CLUSTER_SSH_KEY
    CLUSTER_USER: dbenab100p

RPBS Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /shared/software/conda
    MODULEFILES_PATH: /shared/software/modulefiles
    CLUSTER_HOST: ifb-login.rpbs.univ-paris-diderot.fr
    CLUSTER_SSH_KEY: $RPBS_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools

CIRAD Conda:
  extends:
    - .deploy_conda_tools
    - .deploy_conda_tools_production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /storage/replicated/cirad/binaries/ifb/miniconda3
    MODULEFILES_PATH: /storage/replicated/cirad/binaries/ifb/modulefiles
    CLUSTER_HOST: 193.52.26.138
    CLUSTER_SSH_KEY: $CIRAD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb_agap

####### SINGULARITY PRODUCTION ########

IFB preprod Singularity:
  extends: .deploy_singularity_tools_preproduction

IFB Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /shared/ifbstor1/software/singularity
    MODULEFILES_PATH: /shared/ifbstor1/software/modulefiles
    SOFTWARE_SRC_PATH: /shared/ifbstor1/software/src
    #CLUSTER_HOST: core.cluster.france-bioinformatique.fr
    CLUSTER_HOST: core-login2.cluster.france-bioinformatique.fr
    CLUSTER_SSH_KEY: $IFB_CLUSTER_SSH_KEY
    CLUSTER_USER: root

ABIMS Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /shared/software/singularity
    MODULEFILES_PATH: /shared/software/modulefiles
    SOFTWARE_SRC_PATH: /shared/software/src
    CLUSTER_HOST: slurm0.sb-roscoff.fr
    CLUSTER_SSH_KEY: $ABIMS_CLUSTER_SSH_KEY
    CLUSTER_USER: gitlab-runner

IGBMC Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /shared/software/singularity
    MODULEFILES_PATH: /shared/software/modulefiles
    SOFTWARE_SRC_PATH: /shared/software/src
    CLUSTER_HOST: hpc.igbmc.fr
    CLUSTER_SSH_KEY: $IGBMC_CLUSTER_SSH_KEY
    CLUSTER_USER: cs-hpc

CCUS Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  tags:
    - docker
    - igbmc # needed as it does only allow connection from osiris network
    - XL
  variables:
    SINGULARITY_REPOSITORY_PATH: /usr/local/ifb/singularity
    MODULEFILES_PATH: /usr/local/configfiles/ifb_modules
    CLUSTER_HOST: hpc-login.u-strasbg.fr
    CLUSTER_SSH_KEY: $CCUS_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb

BiRD Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /SINGULARITY/images
    MODULEFILES_PATH: /CONDAS/shares/ifb-nncr/modulesfiles
    SOFTWARE_SRC_PATH: /LAB-DATA/BiRD/apps/ifb-nncr/src
    CLUSTER_HOST: bird2login.univ-nantes.fr
    CLUSTER_SSH_KEY: $BiRD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools

MCIA Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /gpfs/softs/contrib/ifb/singularity
    MODULEFILES_PATH: /gpfs/softs/contrib/ifb/modulefiles
    SOFTWARE_SRC_PATH: /gpfs/softs/contrib/ifb/src
    CLUSTER_HOST: curta.mcia.fr
    CLUSTER_SSH_KEY: $MCIA_CLUSTER_SSH_KEY
    CLUSTER_USER: dbenab100p

RPBS Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /shared/software/singularity
    MODULEFILES_PATH: /shared/software/modulefiles
    SOFTWARE_SRC_PATH: /shared/software/src
    CLUSTER_HOST: ifb-login.rpbs.univ-paris-diderot.fr
    CLUSTER_SSH_KEY: $RPBS_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools

CIRAD Singularity:
  extends:
    - .deploy_singularity_tools
    - .deploy_singularity_tools_production
  variables:
    SINGULARITY_REPOSITORY_PATH: /storage/replicated/cirad/binaries/ifb/singularity
    MODULEFILES_PATH: /storage/replicated/cirad/binaries/ifb/modulefiles
    SOFTWARE_SRC_PATH: /storage/replicated/cirad/binaries/ifb/src
    CLUSTER_HOST: 193.52.26.138
    CLUSTER_SSH_KEY: $CIRAD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb_agap
