#!/bin/bash

version=10.5

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate modeller-${version} 2> /dev/null
if [ $? -ne 0 ]
then
    conda activate ${CONDA_HOME}/envs/modeller-${version}
fi

if [ 'modeller-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load modeller env"
    exit 1;
fi

set -e

sed -i "s/license = r'XXXX'/license = 'MODELIRANJE'/g" ${CONDA_PREFIX}/lib/modeller-10.5/modlib/modeller/config.py 
