#!/bin/bash

EGGNOG_MAPPER_VERSION=2.1.12

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate eggnog-mapper-${EGGNOG_MAPPER_VERSION}
if [ $? -ne 0 ]; then
    conda activate ${CONDA_HOME}/envs/eggnog-mapper-${EGGNOG_MAPPER_VERSION}
fi

if [ "eggnog-mapper-${EGGNOG_MAPPER_VERSION}" != "${CONDA_DEFAULT_ENV}" ]; then
    echo "Unable to load eggnog-mapper-${EGGNOG_MAPPER_VERSION} env"
    exit 1;
fi

set -e

export EGGNOG_DATA_DIR="/shared/bank/eggnog-mapper-${EGGNOG_MAPPER_VERSION}"
mkdir -p "${EGGNOG_DATA_DIR}" && cd "${EGGNOG_DATA_DIR}"

yes | download_eggnog_data.py -P -M -F -f -q
