# AlphaFold 3 prerequisites

Please refer to Google Deepmind's [installation instructions](https://github.com/google-deepmind/alphafold3/blob/main/docs/installation.md)⁠ to get the required databases and the model weights.

## Databases

```bash
git clone https://github.com/google-deepmind/alphafold3.git
cd alphafold3  # Navigate to the directory with cloned AlphaFold 3 repository.
./fetch_databases.sh [<DB_DIR>]
```

## Obtaining Model Parameters

To request access to the AlphaFold 3 model parameters, please complete the form here: <https://github.com/google-deepmind/alphafold3/blob/main/docs/installation.md#obtaining-model-parameters>
