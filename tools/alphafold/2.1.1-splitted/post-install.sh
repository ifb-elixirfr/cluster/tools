#!/bin/bash

version=2.1.1-splitted

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate alphafold-${version} 2> /dev/null
if [ $? -ne 0 ]
then
    conda activate ${CONDA_HOME}/envs/alphafold-${version}
fi

if [ 'alphafold-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load alphafold env"
    exit 1;
fi

set -e

# get launcher
cd ${CONDA_PREFIX}/bin/
wget -N https://raw.githubusercontent.com/reyjul/alphafold/main/docker/run_docker.py
chmod +x ${CONDA_PREFIX}/bin/run_docker.py
