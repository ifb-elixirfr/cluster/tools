BootStrap: docker
From: ubuntu:20.04

%post
    export TOOLS_VERSION=0.3.0
    export PATH=/opt/conda/bin:$PATH

    ## From apt
    export DEBIAN_FRONTEND=noninteractive
    apt-get -qq update && apt-get -qq upgrade -y
    apt-get -qq install -y wget git curl

    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    conda install -q -y -c conda-forge -c bioconda pip python=3.8 medaka openblas spoa racon minimap2 samtools

    # Cleaning
    apt-get -qq autoremove -y
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*
    conda clean -q -y --all

    # Setup
    pip install NGSpeciesID==$TOOLS_VERSION

%environment
    export PATH=/opt/conda/bin:$PATH

%test
    NGSpeciesID --help
    mkdir /tmp/test_ngspeciesID
    cd /tmp/test_ngspeciesID
    curl -LO https://raw.githubusercontent.com/ksahlin/NGSpeciesID/master/test/sample_h1.fastq
    medaka_consensus --help
    NGSpeciesID --ont --fastq sample_h1.fastq --outfolder ./sample_h1 --consensus --medaka
