Bootstrap: docker
From: ubuntu:20.04

# based on https://github.com/yfukasawa/LongQC/blob/master/Dockerfile

%labels
    Author IFB
    Version master

%post
    export PATH=/opt/LongQC:/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive
    # prepare debian/ubuntu
    apt-get update && apt-get upgrade -y
    apt-get install -y  \
            apt-utils \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common \
            sed \
            wget \
            git \
            build-essential \
            libc6-dev \
            zlib1g-dev \
            tar

    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-latest-Linux-x86_64.sh

    # install dependency
    conda update -y conda
    conda install -c conda-forge -y mamba
    mamba install -y -c conda-forge -c bioconda -c defaults\
          numpy \
          pandas'>=0.24.0' \
          scipy \
          jinja2 \
          h5py \
          matplotlib'>=2.1.2' \
          scikit-learn \
          pysam \
          edlib \
          python-edlib

    # get longqc
    cd /opt
    wget https://github.com/yfukasawa/LongQC/archive/refs/tags/1.2.0b.tar.gz
    tar -zxf 1.2.0b.tar.gz
    mv LongQC-1.2.0b LongQC
    cd /opt/LongQC/minimap2-coverage && make
    chmod +x /opt/LongQC/longQC.py
    # should be removed one day
    cd /opt/LongQC/ && sed -i.bak '1 i #!/opt/conda/bin/python' longQC.py

    # Cleanup
    rm -rf /opt/1.2.0b.tar.gz
    #apt remove tar wget git curl
    apt-get clean
    apt-get purge

%environment
    export PATH=/opt/LongQC:/opt/LongQC/minimap2-coverage/:/opt/conda/bin:$PATH

%runscript
    exec longQC.py "$@"

%test
    export PATH=/opt/LongQC:/opt/LongQC/minimap2-coverage/:/opt/conda/bin:$PATH
    longQC.py --version
    longQC.py --help
