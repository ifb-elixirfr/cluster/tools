Bootstrap: docker
From: ubuntu:20.04

%labels
    Author ABiMS
    Version 1.3.2

%files
    /src/hectar/1.3.2/hectar-1.3.2.tar.gz /opt/

%post
    apt-get -qqqq update && apt-get -qqqq upgrade -y
    DEBIAN_FRONTEND=noninteractive apt -y -qqqq install tar gzip locales
    apt -y -qqqq install perl
    apt -y -qqqq install libswitch-perl libmime-base64-perl libparallel-forkmanager-perl
    apt -y -qqqq install gawk libc6-i386 # respectively for signalp, phobius/decodeanhmm
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y -qqqq install ca-certificates-java
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y -qqqq install default-jre # for predisi/java

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    cd /opt/
    tar -zxf hectar-1.3.2.tar.gz
    cd hectar

    rm -rf /opt/hectar-1.3.2.tar.gz
    apt-get clean
    apt-get purge

%environment
    export PATH=/opt/hectar:$PATH
    export LANG=en_US.utf8
    export HECTAR_HMMTOP_ARCH=/opt/hectar/predsoft/hmmtop/hmmtop.arch
    export HECTAR_HMMTOP_PSV=/opt/hectar/predsoft/hmmtop/hmmtop.psv
    export TERM=xterm
    export CLASSPATH=/opt/hectar/predsoft/predisi:/opt/hectar/predsoft/predisi/jspp:$CLASSPATH

%runscript
    echo "HECTAR uses DTU Health Tech SignalP and TargetP that are dedicated to Academic usage and require a license agreement: https://services.healthtech.dtu.dk/software.php"
    exec "$@"

%test
    export PATH=/opt/hectar:$PATH
    export LANG=en_US.utf8
    export HECTAR_HMMTOP_ARCH=/opt/hectar/predsoft/hmmtop/hmmtop.arch
    export HECTAR_HMMTOP_PSV=/opt/hectar/predsoft/hmmtop/hmmtop.psv
    export TERM=xterm
    export CLASSPATH=/opt/hectar/predsoft/predisi:/opt/hectar/predsoft/predisi/jspp:$CLASSPATH
    hectar.pl --help | grep -q "hectar.pl -i input.fasta"
    # Test a single short sequence
    head -n 4 /opt/hectar/test/test_all.fasta > /tmp/single_seq.fasta
    echo "Testing 'hectar.pl -i /tmp/single_seq.fasta'"
    hectar.pl --debug --cpu 1 -i /tmp/single_seq.fasta -d /tmp/hectar_build_test_single 1> /dev/null
    grep -q 'ASGR1_HUMAN' /tmp/hectar_build_test_single/hectar_output.csv
    # Test all test sequences
    echo "Testing 'hectar.pl -i /opt/hectar/test/test_all.fasta'"
    hectar.pl -i /opt/hectar/test/test_all.fasta -d /tmp/hectar_build_test 1> /dev/null
    ( grep -cve '^\s*$' /tmp/hectar_build_test/hectar_output.csv | grep -q 83 ) && echo 'Success!' # Check the number of non empty lines in the output file
