Bootstrap: docker
From: nvidia/cuda:11.8.0-devel-ubuntu18.04

%labels
  Description: Installation de crYOLO 1.9.8 avec CUDA 11 support

%post
    # Mise à jour du système et installation des dépendances
    apt-get update -y -qqqq
    apt-get install -y -qqqq wget libgtk2.0-0 libxxf86vm1 libgl1 #libtiff5 adwaita-icon-theme pkg-config
    apt-get clean

    # Téléchargement et installation de Miniconda v 24.1
    wget -nv https://repo.anaconda.com/miniconda/Miniconda3-py39_24.1.2-0-Linux-x86_64.sh
    chmod +x Miniconda3-py39_24.1.2-0-Linux-x86_64.sh
    ./Miniconda3-py39_24.1.2-0-Linux-x86_64.sh -b -s -p /opt/conda
    rm Miniconda3-py39_24.1.2-0-Linux-x86_64.sh
    export PATH=/opt/conda/bin:$PATH

    # installation des dépendances fixées 
    conda install -q -y -c conda-forge -c anaconda pyqt=5 python=3 numpy=1.18.5 libtiff=4.5.0 wxPython=4.1.1 adwaita-icon-theme=46

    # Installation de nvidia-pyindex et tensorflow version fixée 
    pip -q install 'nvidia-pyindex==1.0.9'
    pip -q install 'nvidia-tensorflow==1.15.5+nv23.3'
    
    # Installation de crYOLO 1.9.8 avec CUDA 11 support
    pip -q install 'cryolo[c11]==1.9.8'

    # Nettoyage
    conda clean -a -y

%environment
    export PATH=/opt/conda/bin:$PATH

%test
    export PATH=/opt/conda/bin:$PATH
    which cryolo_boxmanager.py
    which cryolo_evaluation.py
    which cryolo_gui.py
    which cryolo_predict.py
    which cryolo_train.py
    cryolo_train.py --help