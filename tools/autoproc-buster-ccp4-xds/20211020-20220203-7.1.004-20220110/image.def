BootStrap: docker
From: debian:10

%labels
    Author IGBMC

%files
    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/GPhL_autoPROC_snapshot_20211020.linux64.tar /opt
    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/GPhL_autoPROC_snapshot_20211020_install.sh /opt
    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/autoproc_licence.txt /opt

    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/GPhL_BUSTER_snapshot_20220203.linux64.tar /opt
    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/GPhL_BUSTER_snapshot_20220203_install.sh /opt
    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/buster_licence.txt /opt

    /src/autoproc-buster-ccp4-xds/20211020-20220203-7.1.004-20220110/ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz /opt

%post
    # install system dependencies
    apt-get -y -qqqq update
    apt-get -y -qqqq install bsdtar\
    csh\
    curl\
    ffmpeg\
    grace\
    imagemagick\
    libcgi-pm-perl\
    libfontconfig1-dev\
    libgl1-mesa-dev\
    libglu1-mesa-dev\
    libncurses5\
    libsm6\
    libxcursor1\
    libxcomposite1\
    libxdamage1\
    libxext-dev\
    libxmu6\
    libxrandr2\
    libxrender-dev\
    libxt6\
    libxxf86vm1\
    mesa-utils\
    openbabel\
    openjdk-11-jre\
    php-cli\
    povray\
    pymol\
    tcsh\
    texlive-latex-extra\
    wget

    ## DECTRIS NEGGIA - this should already be included in XDS. In case a bleeding edge version is required,
    ## the lines below can be uncommented 

    ##dependencies to build neggia
    #apt-get -y -qqqq install git\
    #cmake\
    #g++

    ## build & install neggia (XDS plugin for dectris detectors)
    #git clone --branch v1.2.0 https://github.com/dectris/neggia.git
    #cd neggia    
    #mkdir build
    #cd build
    #cmake .. -DCMAKE_BUILD_TYPE=Release
    #cmake --build .
    #cp src/dectris/neggia/plugin/dectris-neggia.so /usr/local/lib64
    #cd ../..
    #rm -rf neggia

    ##
    # CCP4
    ##

    cd /opt
    bsdtar xzf ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz
    touch $HOME/.agree2ccp4v6
    cd /opt/ccp4-7.1
    ./BINARY.setup > /dev/null
    rm /opt/ccp4-*-shelx-arpwarp-linux64.tar.gz

    # load ccp4 environment
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    # create user for gphl software install
    useradd gphl

    ##
    # XDS
    ##

    # install xds dependencies
    apt-get -y -qqqq install\
    build-essential\
    python-pip\
    zlib1g-dev\

    cd /opt
    #wget -q ftp://ftp.mpimf-heidelberg.mpg.de/pub/kabsch/XDS-INTEL64_Linux_x86_64.tar.gz
    wget -q https://xds.mr.mpg.de/XDS-INTEL64_Linux_x86_64.tar.gz
    tar xzf XDS-INTEL64_Linux_x86_64.tar.gz
    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    rm /opt/XDS-INTEL64_Linux_x86_64.tar.gz

    # install XDS-Viewer,  XDSSTAT, XDSCC12, XDSGUI, XSCALE_ISOCLUSTER
    # dependencies first
    apt-get -y -qqqq install\
    libqt4-opengl\
    libqtgui4\
    libqtcore4\
    libqt5test5 libqt5opengl5 libqt5printsupport5\
    xxdiff

    #wget -q https://snapshot.debian.org/archive/debian/20170107T093814Z/pool/main/libp/libpng/libpng12-0_1.2.50-2%2Bdeb8u3_amd64.deb
    #dpkg -i libpng12-0_1.2.50-2+deb8u3_amd64.deb
    #rm libpng12-0_1.2.50-2+deb8u3_amd64.deb

    mkdir -p /opt/xds_gui && cd /opt/xds_gui
    wget -N -r -np -nd -e robots=off -R "index.html*" -R "robots*" http://strucbio.biologie.uni-konstanz.de/pub/linux_bin/ || true
    chmod a+x *
    ln -s xdscc12 xscalecc12
    ln -s XDS-viewer xdsviewer
    ln -s XDS-viewer xds-viewer
    export PATH=/opt/xds_gui:$PATH

    ##
    # ADXV
    ##

    # install adxv (without GL)
    cd /opt
    mkdir adxv
    cd adxv
    wget -q https://www.scripps.edu/tainer/arvai/adxv/adxv_1.9.13/adxv.x86_64CentOS6
    mv adxv.x86_64CentOS6 adxv
    chmod +x adxv
    export PATH=/opt/adxv:$PATH

    ##
    # MOSFLM
    ##

    # install mosflm
    cd /opt
    mkdir mosflm
    cd mosflm
    wget -q https://www.mrc-lmb.cam.ac.uk/harry/mosflm/ver722/pre-built/mosflm-linux-64-noX11.zip
    unzip -qq mosflm-linux-64-noX11.zip
    mv mosflm-linux-64-noX11 mosflm
    chmod +x mosflm
    export PATH=/opt/mosflm:$PATH

    # install gemmi (https://gemmi.readthedocs.io)
    pip -q install pybind11
    pip -q install gemmi

    ##
    # AUTOPROC
    ##

    # build and install autoproc
    cd /opt
    mkdir GPhL_autoPROC
    cd GPhL_autoPROC
    UPSTREAM_BUNDLE="GPhL_autoPROC_snapshot_20211020"
    mv /opt/${UPSTREAM_BUNDLE}.linux64.tar .
    mv /opt/${UPSTREAM_BUNDLE}_install.sh .
    mv /opt/autoproc_licence.txt .licence
    chmod +x ${UPSTREAM_BUNDLE}_install.sh
    chown -R gphl:gphl .
    su -c "./${UPSTREAM_BUNDLE}_install.sh > /dev/null" gphl
    chown -R root:root .
    rm /opt/GPhL_autoPROC/${UPSTREAM_BUNDLE}.linux64.tar
    rm /opt/GPhL_autoPROC/${UPSTREAM_BUNDLE}_install.sh

    ##
    # BUSTER
    ##

    # load ccp4 environment
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    # install buster dependencies
    apt-get -y -qqqq install bsdtar csh curl ffmpeg grace imagemagick \
    libcgi-pm-perl libfontconfig1-dev libgl1-mesa-dev libglu1-mesa-dev libncurses5 \
    libsm6 libxcursor1 libxcomposite1 libxdamage1 libxext-dev libxmu6 libxrandr2 \
    libxrender-dev libxt6 libxxf86vm1 mesa-utils openbabel openjdk-11-jre php-cli \
    povray pymol tcsh texlive-latex-extra

    # build and install buster
    mkdir /opt/GPhL_BUSTER
    cd /opt/GPhL_BUSTER
    UPSTREAM_BUNDLE=GPhL_BUSTER_snapshot_20220203
    mv /opt/${UPSTREAM_BUNDLE}.linux64.tar .
    mv /opt/${UPSTREAM_BUNDLE}_install.sh .
    mv /opt/buster_licence.txt .licence
    #Disabling root user check
    sed -i -e "/trying installation under/ c\  echo Installing under root account anyway" ${UPSTREAM_BUNDLE}_install.sh
    chmod +x ${UPSTREAM_BUNDLE}_install.sh
    ./${UPSTREAM_BUNDLE}_install.sh > /dev/null

    #fix for weird alias-related error messages 'alias: could not parse "pushd $CCP4>/dev/null": 1:12: > is not a valid word'
    head -n -13 /opt/ccp4-7.1/bin/ccp4.setup-sh >/ccp4.setup-sh
    mv /ccp4.setup-sh /opt/ccp4-7.1/bin/ccp4.setup-sh


    # cleanup for buster
    rm /opt/GPhL_BUSTER/${UPSTREAM_BUNDLE}.linux64.tar
    rm /opt/GPhL_BUSTER/${UPSTREAM_BUNDLE}_install.sh

    ##
    # ALL
    ##

    # cleanup
    apt-get remove -y bsdtar wget
    rm -rf /var/lib/apt/lists/*

%environment
    export LC_ALL=C

    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    export PATH=/opt/xds_gui:$PATH
    export PATH=/opt/adxv:$PATH
    export PATH=/opt/mosflm:$PATH
    . /opt/GPhL_autoPROC/setup.sh

    export PATH=/opt/CCDC/CSD_2019/bin:$PATH
    export BDG_TOOL_MOLPROBITY_ROOT=/opt/MolProbity
    #export BDG_TOOL_MOGUL=/opt/CCDC/CSD_2019/bin/mogul
    . /opt/GPhL_BUSTER/setup.sh


%test
    export LC_ALL=C

    # check ccp4
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash
    ccp4-python --version 2>&1 | grep "Python 2.7"

    # check autoproc
    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    export PATH=/opt/adxv:$PATH
    export PATH=/opt/mosflm:$PATH
    . /opt/GPhL_autoPROC/setup.sh
    process -checkdeps

    # check buster
    export BDG_TOOL_MOLPROBITY_ROOT=/opt/MolProbity
    export PATH=/opt/CCDC/CSD_2019/bin:$PATH
    #export BDG_TOOL_MOGUL=/opt/CCDC/CSD_2019/bin/mogul
    . /opt/GPhL_BUSTER/setup.sh
    # Test disabled because:
    ##  Setting tool 'corr' to '/opt/GPhL_BUSTER/autoBUSTER/bin/linux64/corr' from your $PATH
    ## ERROR: there are 1 problems with external tools. See lines beginning "ERROR" above.
    ## See /opt/GPhL_BUSTER/docs/installation/index.html for help
    #buster-report -checkdeps

    #very basic test for xdsgui
    test -f /opt/xds_gui/xdsgui

